import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://backoffice.grup01.ddaw.site/api'
export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        pedidos: [],
        estados: [],
        loaded: false
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        getPedidoById: (state) => (id) => {
            return state.pedidos.find(producto => producto.id == id);
        }
    },
    actions: {
        retrieveName(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.get('/user')
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        loadData(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if (!context.state.loaded) {
                return new Promise(() => {
                    axios.get('/pedidos-repartidor')
                        .then(response => {
                            context.commit('loadPedidos', response.data.data)
                            context.commit("loadState", true);
                        })
                        .catch(response => {
                            alert(response)
                        })
                    axios.get('/estados')
                        .then(response => {
                            context.commit('loadEstados', response.data.data)
                            context.commit("loadState", true);
                        })
                        .catch(response => {
                            alert(response)
                        })
                })
            }
        },
        changeEstado(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.put('/cambio-estado/' + data.id, {
                        id_estado: data.estado
                    })
                    .then(response => {
                        context.commit('loadEstado', response.data)
                        resolve(response)
                    })
                    .catch(response => {
                        reject(response)
                    })
            })
        },
        eliminar(context, data) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.delete('/pedidos/' + data.id)
                    .then(response => {
                        context.commit('loadDelete', data.id)
                        resolve(response)
                    })
                    .catch(response => {
                        reject(response)
                    })
            })
        },
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.get('/logout')
                        .then(response => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            reject(error)
                        })
                })
            }
        },
        retrieveToken(context, credentials) {

            return new Promise((resolve, reject) => {
                axios.post('/login', {
                        email: credentials.username,
                        password: credentials.password,
                    })
                    .then(response => {
                        const token = response.data.access_token

                        localStorage.setItem('access_token', token)
                        context.commit('retrieveToken', token)
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        reject(error)
                    })
            })
        },
    },
    mutations: {
        loadEstados(state, estados) {
            state.estados = estados
        },
        loadState(state, isLaoded) {
            state.loaded = isLaoded
        },
        loadDelete(state, id) {
            state.pedidos = state.pedidos.filter(element => element.id !== id)
        },
        loadEstado(state, pedido) {
            let element = state.pedidos.find(element => element.id == pedido.data.id);
            element.estado.id = pedido.data.estado.id;
            element.estado.name = pedido.data.estado.name;
        },
        loadPedidos(state, pedidos) {
            state.pedidos = pedidos
        },
        loadUsers(state, users) {
            state.users = users
        },
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        }
    }
})