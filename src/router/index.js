import Vue from 'vue'
import Router from 'vue-router'
import PedidosList from '../components/pedidos/PedidosList'
import FormPedido from '../components/pedidos/FormPedido'
import PedidosDetail from '../components/pedidos/PedidosDetail'
import Login from '../components/auth/Login'
import Logout from '../components/auth/Logout'

Vue.use(Router)

export default new Router({

  mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: PedidosList,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/pedido/:id',
    name: 'pedido',
    component: PedidosDetail,
    props: true,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/editar-pedido/:id',
    name: 'editar-pedido',
    component: FormPedido,
    props: true,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout,
    meta: {
      requiresAuth: true,
    }
  }
  ]
})