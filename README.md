# vue-repartidor

## Descripción

Aplicacion para batoilogig para movil, desarrollado en vue. Vista hecha para movil y tablet

## Dependencias

        "axios": "^0.19.2",
        "bootstrap-vue": "^2.5.0",
        "core-js": "^3.6.4",
        "mutationobserver-shim": "^0.3.3",
        "router": "^1.3.4",
        "vee-validate": "^2.2.15",
        "vue": "^2.6.10",
        "vue-router": "^3.1.5",
        "vuex": "^3.1.2"

## Acceso

staging: user: carmelo24@example.org password:12345678


